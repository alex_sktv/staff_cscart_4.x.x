<?php

use Tygh\Enum\ProductFeatures;
use Tygh\Navigation\LastView;
use Tygh\Tools\SecurityHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Removes staff member by identifier
 *
 * @param int $staff_id - Staff identifier
 * @return bool - True on success, false otherwise
 */
function fn_delete_staff($staff_id)
{
    $staff_deleted = false;

    if (!empty($staff_id)) {
        db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
        fn_delete_image_pairs($staff_id, 'staff');
        $staff_deleted = true;
    }

    return $staff_deleted;
}

/**
 * Returns an array of staff members using some params with pagination
 *
 * @param array $params - Array of params
 * @param int $items_per_page - Items per page
 * @param $lang_code - Language code
 * @return array - ($staff, $params)
 */
function fn_get_staff($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
    $params = LastView::instance()->update('staff', $params);

    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $fields = array (
        '?:staff.staff_id',
        "IF(?:staff.firstname!='', ?:staff.firstname, ?:users.firstname) AS firstname",
        "IF(?:staff.lastname!='', ?:staff.lastname, ?:users.lastname) AS lastname",
        "IF(?:staff.email!='', ?:staff.email, ?:users.email) AS email",
        '?:staff.function',
        '?:staff.position',
        '?:staff.status',
        '?:staff.user_id',
    );

    $join = 'LEFT JOIN ?:users ON ?:users.user_id = ?:staff.user_id';
    $order = 'ORDER BY ?:staff.position, ?:staff.staff_id DESC';
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:staff $join");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $data = db_get_array("SELECT " . implode(', ', $fields) . " FROM ?:staff $join $order $limit ");

    return array($data, $params);
}

/**
 * Updates staff member
 *
 * @param array $staff_data - Staff member data array
 * @param int $staff_id - Staff identifier (empty if we're adding the new staff member)
 * @param string $lang_code - Language code to add/update staff for
 * @return int - ID of the added/updated staff member
 */
function fn_update_staff($staff_data, $staff_id = 0, $lang_code = DESCR_SL)
{
    SecurityHelper::sanitizeObjectData('staff', $staff_data);

    if (!empty($staff_data['user_id'])) {
        if (!db_get_field("SELECT COUNT(*) FROM ?:users LEFT JOIN ?:staff ON ?:staff.user_id = ?:users.user_id "
                        . "WHERE ?:users.user_id = ?i AND (?:staff.staff_id IS NULL OR ?:staff.staff_id = ?i)", $staff_data['user_id'], $staff_id)) {
            $staff_data['user_id'] = 0;
        }
    }

    // Add new
    if (empty($staff_id)) {
        $staff_data['staff_id'] = $staff_id = db_query('INSERT INTO ?:staff ?e', $staff_data);
        fn_attach_image_pairs('staff_image', 'staff', $staff_id, $lang_code);

    // Update
    } else {
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $staff_data, $staff_id);
        fn_attach_image_pairs('staff_image', 'staff', $staff_id, $lang_code);
    }

    return $staff_id;
}

/**
 * Returns a staff member data by identifier
 *
 * @param int $staff_id - Staff memger identifier
 * @param $lang_code - Language code
 * @return array - Staff member firstname, lastname etc
 */
function fn_get_staff_data($staff_id, $lang_code = DESCR_SL)
{
    $staff = array();

    if (!empty($staff_id)) {
        $staff = db_get_row("SELECT * FROM ?:staff WHERE staff_id = ?i", $staff_id);

        if (!empty($staff)) {
            $staff['image_pair'] = fn_get_image_pairs($staff_id, 'staff', 'M', true, true, $lang_code);
        }
    }

    return $staff;
}

/**
 * Gets all staff members for block
 *
 * @return array Found employers.
 */
function fn_get_all_staff()
{
    $fields = array (
        '?:staff.staff_id',
        "IF(?:staff.firstname!='', ?:staff.firstname, ?:users.firstname) AS firstname",
        "IF(?:staff.lastname!='', ?:staff.lastname, ?:users.lastname) AS lastname",
        "IF(?:staff.email!='', ?:staff.email, ?:users.email) AS email",
        '?:staff.function',
    );

    $join = 'LEFT JOIN ?:users ON ?:users.user_id = ?:staff.user_id';
    $order = 'ORDER BY ?:staff.position, ?:staff.staff_id DESC';
    $condition = "?:staff.status='A'";

    $staff = db_get_array("SELECT " . implode(', ', $fields) . " FROM ?:staff $join $order");

    foreach ($staff as &$_staff) {
        $_staff['image_pair'] = fn_get_image_pairs($_staff['staff_id'], 'staff', 'M', true, true);
    }

    return $staff;
}

/**
 * Gets staff member's email by identifier.
 *
 * @param int $staff_id - Staff memger identifier
 * @return sting - Email address.
 */
function fn_get_staff_email($staff_id)
{
    $staff_email = '';

    if (!empty($staff_id)) {
        $staff_email = db_get_field("SELECT IF(?:staff.email != '', ?:staff.email, ?:users.email) "
                                  . "FROM ?:staff LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id WHERE ?:staff.staff_id = ?i", $staff_id);
    }

    return $staff_email;
}
