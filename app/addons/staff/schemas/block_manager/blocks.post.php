<?php

$schema['staff'] = array(
    'templates' => array(
        'addons/staff/blocks/staff.tpl' => array(
            'settings' => array(
                'not_scroll_automatically' => array (
                    'type' => 'checkbox',
                    'default_value' => 'N'
                ),
                'scroll_per_page' =>  array (
                    'type' => 'checkbox',
                    'default_value' => 'N'
                ),
                'speed' =>  array (
                    'type' => 'input',
                    'default_value' => 400
                ),
                'pause_delay' =>  array (
                    'type' => 'input',
                    'default_value' => 3
                ),
                'item_quantity' =>  array (
                    'type' => 'input',
                    'default_value' => 6
                ),
                'thumbnail_width' =>  array (
                    'type' => 'input',
                    'default_value' => 50
                ),
                'outside_navigation' => array (
                    'type' => 'checkbox',
                    'default_value' => 'Y'
                ),
            ),
        ),
    ),
    'wrappers' => 'blocks/wrappers',
    'content' => array(
        'staff' => array(
            'type' => 'function',
            'function' => array('fn_get_all_staff'),
        )
    ),
    'cache' => array(
        'update_handlers' => array(
            'staff',
            'images_links',
        )
    ),
);

return $schema;
