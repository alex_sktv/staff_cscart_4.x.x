<?php

$schema['central']['website']['items']['staff'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'staff.manage',
    'position' => 2000,
);

return $schema;

