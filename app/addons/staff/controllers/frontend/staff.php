<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'email') {
        if (defined('AJAX_REQUEST')) {
            Tygh::$app['view']->assign('staff_id', $_REQUEST['staff_id']);
            Tygh::$app['view']->assign('email', fn_get_staff_email($_REQUEST['staff_id']));
            return array(CONTROLLER_STATUS_OK);
        }
    }
}

return array(CONTROLLER_STATUS_NO_PAGE);

