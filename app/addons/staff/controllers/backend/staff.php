<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_define('KEEP_UPLOADED_FILES', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $suffix = '';

    if ($mode == 'update') {
        fn_trusted_vars('staff_data', 'regexp');
        $staff_id = fn_update_staff($_REQUEST['staff_data'], $_REQUEST['staff_id'], DESCR_SL);
        $suffix = ".manage";
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['staff_id'])) {
            fn_delete_staff($_REQUEST['staff_id']);
        }
        $suffix = ".manage";
    }

    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);

//
// Staff list
//
} elseif ($mode == 'manage') {
    $params = $_REQUEST;

    list($staff, $search) = fn_get_staff($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);

    Tygh::$app['view']->assign('staff', $staff);
    Tygh::$app['view']->assign('search', $search);
    Tygh::$app['view']->assign('object', 'global');

    if (empty($staff) && defined('AJAX_REQUEST')) {
        $ajax->assign('force_redirection', fn_url('staff.manage'));
    }

//
// Update staff
//
} elseif ($mode == 'update') {

    $s_data = fn_get_staff_data($_REQUEST['staff_id']);

    if (isset($_REQUEST['object'])) {
        Tygh::$app['view']->assign('object', $_REQUEST['object']);
    }

    Tygh::$app['view']->assign('staff_data', $s_data);
    Tygh::$app['view']->assign('staff_id', $_REQUEST['staff_id']);

}

