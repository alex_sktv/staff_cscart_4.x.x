{$obj_prefix = "`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel ty-scroller-list">
    {foreach from=$staff item="st" name="for_staff"}
        <div class="ty-scroller-list__item">
            <div class="ty-scroller-list__img-block">
                {include file="common/image.tpl" assign="object_img" image_width=$block.properties.thumbnail_width image_height=$block.properties.thumbnail_width images=$st.image_pair no_ids=true lazy_load=true obj_id="scr_`$block.block_id`000`$st.staff_id`"}
                {$object_img nofilter}
            </div>
            <div class="ty-scroller-list__description ty-strong">{$st.firstname} {$st.lastname}</div>
            <div class="ty-scroller-list__description">{$st.function}</div>
            <div id="email_contents_{$st.staff_id}">
                <a href="{"staff.email?staff_id=`$st.staff_id`"|fn_url}" class="cm-ajax cm-post" data-ca-target-id="email_contents_{$st.staff_id}">{__("staff.show_email")}</a>
            </div>
         </div>
   {/foreach}
</div>

{include file="common/scroller_init.tpl" items=$brands prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
