{capture name="adv_buttons"}
    {capture name="add_new_picker"}
        {include file="addons/staff/views/staff/update.tpl" option_id="0"}
    {/capture}
    {include file="common/popupbox.tpl" id="add_new_staff" text=__("staff.new") title=__("staff.add") act="general" content=$smarty.capture.add_new_picker icon="icon-plus"}
{/capture}

{capture name="mainbox"}

    {include file="common/pagination.tpl"}

    <div class="items-container cm-sortable" data-ca-sortable-table="staff" data-ca-sortable-id-name="staff_id" id="manage_staff_list">
        {if $staff}
        <table width="100%" class="table table-middle table-objects table-striped">
        <thead>
        <tr>
            <th width="1%"></th>
            <th>{__("first_name")} {__("and")} {__("last_name")}</th>
            <th>{__("staff.function")}</th>
            <th>{__("email")}</th>
            <th>{__("user")}</th>
            <th>&nbsp;</th>
            <th class="right">{__("status")}</th>
        </tr>
        </thead>
        <tbody>
            {foreach from=$staff item=st}
             <tr class="cm-row-status-{$st.status|lower} cm-sortable-row cm-sortable-id-{$st.staff_id} cm-row-item" data-ct-staff="{$st.staff_id}">
                <td width="1%" class="no-padding-td">
                    <span class="handler cm-sortable-handle"></span></td>
                <td>
                    <a class="row-status cm-external-click" data-ca-external-click-id="opener_group_staff_{$st.staff_id}">{$st.firstname|default:" - "} {$st.lastname|default:" - "}</a></td>
                <td>
                    {$st.function}</td>
                <td>
                    <a href="mailto:{$st.email}">{$st.email}</a></td>
                <td>
                    {if !empty($st.user_id)}<a href="{"profiles.update&user_id=`$st.user_id`"|fn_url}">{$st.user_id}</a>
                    {else} - {/if}</td>
                <td class="nowrap right">
                    {capture name="tools_list"}
                        <li>{include file="common/popupbox.tpl" id="group_staff_`$st.staff_id`" text="{__("staff.editing")}: `$st.firstname` `$st.lastname`" act="edit" link_text=$link_text href="staff.update?staff_id=`$st.staff_id`" no_icon_link=true}</li>
                        <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="staff.delete?staff_id=`$st.staff_id`"}</li>
                    {/capture}
                    <div class="hidden-tools">
                        {dropdown content=$smarty.capture.tools_list}
                    </div></td>
                <td class="right" width="10%">
                    {include file="common/select_popup.tpl" id=$st.staff_id status=$st.status items_status="staff"|fn_get_predefined_statuses object_id_name="staff_id" table="staff" popup_additional_class="dropleft"}</td>
            </tr>
            {/foreach}
        </tbody>
        </table>
        {else}
        <p class="no-items">{__("no_data")}</p>
        {/if}
    <!--staff_list--></div>

    {include file="common/pagination.tpl"}

{/capture}

{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons select_language=true}
