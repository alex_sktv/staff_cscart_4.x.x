{if $staff_data.staff_id}
    {assign var="id" value=$staff_data.staff_id}
{else}
    {assign var="id" value=0}
{/if}

{assign var="allow_save" value=$staff_data|fn_allow_save_object:"staff"}

<div id="content_group_staff_{$id}">

<form action="{""|fn_url}" method="post" name="staff_form_{$id}" class="form-horizontal form-edit form-highlight cm-disable-empty-files {if !$allow_save}cm-hide-inputs{/if}" enctype="multipart/form-data">
<input type="hidden" name="staff_id" value="{$id}" />

<div class="cm-tabs-content" id="tabs_content_{$id}">
    <div id="content_tab_staff_details_{$id}">
    <fieldset>
        <div class="control-group">
            <input class="cm-no-hide-input" type="hidden" value="{$object}" name="object">
            <label for="elm_staff_firstname_{$id}" class="control-label">{__("first_name")}</label>
            <div class="controls">
            <input class="span9" type="text" name="staff_data[firstname]" id="elm_staff_firstname_{$id}" value="{$staff_data.firstname}"/>
            </div>
        </div>

        <div class="control-group">
            <input class="cm-no-hide-input" type="hidden" value="{$object}" name="object">
            <label for="elm_staff_lastname_{$id}" class="control-label">{__("last_name")}</label>
            <div class="controls">
            <input class="span9" type="text" name="staff_data[lastname]" id="elm_staff_lastname_{$id}" value="{$staff_data.lastname}"/>
            </div>
        </div>

        <div class="control-group">
            <input class="cm-no-hide-input" type="hidden" value="{$object}" name="object">
            <label for="elm_staff_email_{$id}" class="control-label cm-email">{__("email")}</label>
            <div class="controls">
            <input class="span9" type="text" name="staff_data[email]" id="elm_staff_email_{$id}" value="{$staff_data.email}"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="elm_staff_function_{$id}">{__("staff.function")}</label>
            <div class="controls">
            <textarea id="elm_staff_function_{$id}" name="staff_data[function]" cols="55" rows="8" class="span9">{$staff_data.function}</textarea>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="elm_image_{$id}">{__("image")}</label>
            <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff_image" image_key=$id hide_titles=true no_detailed=true image_object_type="staff" image_type="M" image_pair=$staff_data.image_pair prefix=$id}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="elm_user_id_{$id}">{__("user")}</label>
            <div class="controls">
            <input type="text" name="staff_data[user_id]" id="elm_user_id_{$id}" value="{$staff_data.user_id}" size="3" class="input-small" />
            </div>
        </div>
        
        {hook name="staff:properties"}
        {/hook}
    </fieldset>
    <!--content_tab_staff_details_{$id}--></div>

</div>

<div class="buttons-container">
    {if $id}
        {if !$allow_save && $shared_product != "Y"}
            {assign var="hide_first_button" value=true}
        {/if}
    {/if}
    {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" cancel_action="close" extra="" hide_first_button=$hide_first_button save=$id}
</div>

</form>

<!--content_group_staff_{$id}--></div>
